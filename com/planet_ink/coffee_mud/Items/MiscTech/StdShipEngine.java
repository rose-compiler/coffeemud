package com.planet_ink.coffee_mud.Items.MiscTech;
import com.planet_ink.coffee_mud.core.interfaces.*;
import com.planet_ink.coffee_mud.core.*;
import com.planet_ink.coffee_mud.core.collections.*;
import com.planet_ink.coffee_mud.Abilities.interfaces.*;
import com.planet_ink.coffee_mud.Areas.interfaces.*;
import com.planet_ink.coffee_mud.Behaviors.interfaces.*;
import com.planet_ink.coffee_mud.CharClasses.interfaces.*;
import com.planet_ink.coffee_mud.Commands.interfaces.*;
import com.planet_ink.coffee_mud.Common.interfaces.*;
import com.planet_ink.coffee_mud.Exits.interfaces.*;
import com.planet_ink.coffee_mud.Items.interfaces.*;
import com.planet_ink.coffee_mud.Locales.interfaces.*;
import com.planet_ink.coffee_mud.MOBS.interfaces.*;
import com.planet_ink.coffee_mud.Races.interfaces.*;

import java.util.*;

/* 
   Copyright 2000-2013 Bo Zimmerman

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
public class StdShipEngine extends StdElecItem implements ShipComponent.ShipEngine
{
	public String ID(){	return "StdShipEngine";}
	public StdShipEngine()
	{
		super();
		setName("a ships engine");
		basePhyStats.setWeight(500);
		setDisplayText("a ships engine sits here.");
		setDescription("");
		baseGoldValue=500000;
		basePhyStats().setLevel(1);
		recoverPhyStats();
		setMaterial(RawMaterial.RESOURCE_STEEL);
		setFuelType(RawMaterial.RESOURCE_PLASMA);
	}
	public boolean sameAs(Environmental E)
	{
		if(!(E instanceof StdShipEngine)) return false;
		return super.sameAs(E);
	}
	protected int maxThrust=1000;
	public int getMaxThrust(){return maxThrust;}
	public void setMaxThrust(int max){maxThrust=max;}
	protected int thrust=1000;
	public int getThrust(){return thrust;}
	public void setThrust(int current){thrust=current;}
	
	public void affectPhyStats(Physical affected, PhyStats affectableStats)
	{
		super.affectPhyStats(affected, affectableStats);
		if(affected instanceof Room)
			affectableStats.setSensesMask(affectableStats.sensesMask()|PhyStats.SENSE_ROOMCIRCUITED);
	}
	
	
	public void executeMsg(Environmental myHost, CMMsg msg)
	{
		super.executeMsg(myHost, msg);
		if(msg.amITarget(this))
		{
			if(msg.sourceMinor()==CMMsg.TYP_POWERCURRENT) 
			{
			}
		}
	}
	
	public void destroy()
	{
		CMLib.threads().deleteTick(this,Tickable.TICKID_ELECTRONICS);
		super.destroy();
	}
	
	public void setOwner(ItemPossessor newOwner)
	{
		final ItemPossessor prevOwner=super.owner;
		super.setOwner(newOwner);
		if(prevOwner != newOwner)
		{
			if(newOwner instanceof Room)
			{
				if(!CMLib.threads().isTicking(this, Tickable.TICKID_ELECTRONICS))
					CMLib.threads().startTickDown(this, Tickable.TICKID_ELECTRONICS, 1);
			}
			else
			{
				CMLib.threads().deleteTick(this,Tickable.TICKID_ELECTRONICS);
			}
		}
	}
	
	public boolean tick(Tickable ticking, int tickID)
	{
		if(!super.tick(ticking, tickID))
			return false;
		if(tickID==Tickable.TICKID_ELECTRONICS)
		{
			if(activated())
			{
			}
		}
		return true;
	}
	
}
